# Prueba Frontend Dante G Morales - Yaydoo

Aplicación creada con React JS usando componentes funcionales, Hooks y Redux, que permite el login y mostrar los post del usuario.

## Link del proyecto Gitlab

https://gitlab.com/dantegmorales/prueba-frontend-yaydoo-dantegm 

## Comandos para ejecutar el proyecto

En el directorio raiz del proyecto correr los siguientes comandos:

### `npm install`

### `npm start`

Corre la app en modo desarrollo:
Abre [http://localhost:3000](http://localhost:3000) para ver en el navergador.


### By Dante G Morales 👽