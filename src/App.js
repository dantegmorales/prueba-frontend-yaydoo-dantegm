import React from 'react';
import { Provider } from 'react-redux';
import {themeConfig} from "./styles/themeConfig";
import {ThemeProvider} from "@material-ui/core/styles";
import { AppRouter } from './router/AppRouter';
import { store } from "./store/store";

export const App = () => {

  /*Componente principal de la aplicación que carga el componente de rutas y 
  provee el store de Redux y el theme de material UI a todos los componentes de la app*/
  return (
    <Provider store={store}>
      <ThemeProvider theme={themeConfig}>
          <AppRouter/>
      </ThemeProvider>
    </Provider>
  )
}
