import { types } from "../types/types";
import { authUser } from '../helpers/auth/authUser';

//Acction para autenticación
export const loginEmailPassword = (formValues)=>{
    return async (dispatch)=>{
        const{ email,password }=formValues;
        const resp = await authUser(email,password);
        if(resp.token)dispatch(login(email,resp.token));
    }
}
//Acción para disparar login
export const login = (email,token)=>({
    type:types.authLogin,
    payload:{
        email,
        token
    }
});
//Acción para disparar logout
export const logout = ()=>({
    type:types.authLogout,
});