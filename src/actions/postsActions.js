import { getPostByUser } from "../helpers/dashboard/users";
import { types } from "../types/types";

export const loadPosts = (userId)=>{
    return async (dispatch)=>{
        const posts = await getPostByUser(userId);
        dispatch(setPosts(posts));
    }
}

export const setPosts = (post)=>({
    type:types.postsAdd,
    payload:post
})

export const deletePost = (id)=>({
    type:types.postsDelete,
    payload:id,
})