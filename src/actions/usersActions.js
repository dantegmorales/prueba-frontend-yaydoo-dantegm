import { getUsers } from "../helpers/dashboard/users";
import { types } from "../types/types";

export const loadUsers = ()=>{
    return async (dispatch)=>{
        const users = await getUsers();
        console.log('usuarios del action:',users);
        dispatch(setUsers(users));
    }
}

export const setUsers = (users)=>({
    type:types.usuariosAdd,
    payload:users
})

export const setUserSelected = (user)=>({
    type:types.usuarioSelected,
    payload:user
})