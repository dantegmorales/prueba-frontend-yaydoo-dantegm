import { Button, Grid, IconButton, Paper, TextField, Typography } from '@material-ui/core';
import { Mail } from '@material-ui/icons';
import React from 'react';
import { useDispatch } from 'react-redux';
import Swal from 'sweetalert2';
import { loginEmailPassword } from '../../actions/autenticacion';
import { useForm } from "../../hooks/useForm";
import { loginStyles } from "../../styles/loginStyles";

export const Login = () => {
    //Usamos el dispatch para realizar acciones
    const dispatch = useDispatch();
    //Instanciamos el objeto de clases
    const classes =loginStyles();
    //Usamos nuestro custom hook para manejo de states del formulario
    const[formLoginValues,handleInputChage,reset] = useForm({
        email:'',
        password:'',
    })
    //Desestructuramos los valores del formulario
    const {email,password}=formLoginValues;

    //Función para el Submit de datos
    const handleLoginSubmit =(e)=>{
        e.preventDefault();
        if(email==='') return Swal.fire({
            text: 'El campo email no puede ir vacio',
            icon: 'error',
            confirmButtonText: 'Entendido'});
        if(password==='') return Swal.fire({
            text: 'El campo password no puede ir vacio',
            icon: 'error',
            confirmButtonText: 'Entendido'});
        dispatch(loginEmailPassword(formLoginValues));
    }

    return (
        <div className={classes.root}>
            <Grid 
            className={classes.container}
            container
            direction="column"
            justifyContent="center"
            alignItems="center">
                <Grid className={classes.titulo} item xs={12}>
                    <IconButton >
                        <Mail/>
                    </IconButton>
                    <Typography 
                        variant="h5"
                    >
                        Welcome back!
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Paper className={classes.formContainer}>
                        <form onSubmit={handleLoginSubmit}>
                            <TextField
                            className={classes.inputs}
                            fullWidth
                            variant="outlined"
                            label="Email Address"
                            name="email"
                            value={email}
                            onChange={handleInputChage}
                             />
                            <TextField
                            className={classes.inputs}
                            fullWidth
                            variant="outlined"
                            label="Password"
                            name="password"
                            value={password}
                            onChange={handleInputChage}
                            />
                            <Button 
                            className={classes.btnLogin}
                            type="submit"
                            fullWidth
                            variant="contained" 
                            color="primary">
                                Login
                            </Button>
                        </form>
                    </Paper>
                </Grid>
                <Grid className={classes.footerContainer} item  xs={12}>
                    <Grid container className={classes.footer}>
                        <Grid item xs={6}>
                            Forgot your password
                        </Grid>
                        <Grid item xs={6}>
                            Don't have an account? Get Started
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}
