import { Button, Card, CardActions, CardContent, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
import React,{useState,useEffect} from 'react'
import { useSelector } from 'react-redux';
import { getAlbumsByUser } from "../../helpers/dashboard/users";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

export const Album = () => {

    //Obtenemos el usuario seleccionado con el selector
    const {userSelected} = useSelector(state => state.users);
    //States para albums
    const [albums, setAlbums] = useState({})
    //Efecto para cargar los albums del usuario
    useEffect(async() => {
        const alb =  await getAlbumsByUser(userSelected.id);
        console.log('resultado de la petición',alb);
        setAlbums(alb);
    }, []);

    //Clasess
    const classes = useStyles();

    //Condicional en caso de no existir un  usuario seleccionado.
    if(Object.entries(userSelected).length === 0)return(<div><Typography>No hay ningun usuario seleccionado.</Typography></div>)

    return (
        <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
            <Typography variant="h4">
                Albums de: {userSelected.first_name} -{userSelected.last_name}
            </Typography>
        </Grid>
        <Grid item xs={12}>
            {
               (albums.length>0)&&
               albums.map(alb=>(
                <Card className={classes.root} variant="outlined">
                <CardContent>
                  <Typography variant="h5" component="h2">
                    {alb.title}
                  </Typography>
                  <Typography variant="body2" component="p">
                    ...
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small">
                      Ver Album
                    </Button>
                </CardActions>
              </Card>
              ))
          }
        </Grid>
      </Grid>
    </div>
    )
}
