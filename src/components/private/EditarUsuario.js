import { Avatar, Button, Card, CardActionArea, CardActions, CardContent, IconButton, List, ListItem, ListItemIcon, ListItemText, makeStyles, TextField, Tooltip, Typography } from '@material-ui/core'
import { Delete } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deletePost, loadPosts } from '../../actions/postsActions';
import { getPostByUser } from '../../helpers/dashboard/users';

const useStyles = makeStyles((theme)=>({
    card: {
      maxWidth: 345,
      marginBottom:'30px',
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
      },
      root: {
        flexGrow: 1,
      },
  }));

export const EditarUsuario = ({userEdit}) => {
     //Instanciamos estilos
     const classes = useStyles();
     //Instanciamos el dispatch
      const dispatch = useDispatch();
    //Selector para los post
    const {posts} = useSelector(state => state.posts)
    //State para valores del formulario
    const [formEditar, setFormEditar] = useState({
        name:"",
        last:"",
        job:"",
    });
    console.log('posts de usuarios:',posts)
    //Efecto al llegar nuevo usuario
    useEffect(async() => {
       
        dispatch(loadPosts(userEdit.id))
        setFormEditar({
            name:userEdit.first_name,
            last:userEdit.last_name,
            email:userEdit.email,
            job:'',
        })
    }, [userEdit]);

    //Desestructuramos arreglo
    const {name,last,email,job} = formEditar;
    //Funcion para controlar datos del formulario
    const handleInputChage = ({target})=>{
        setFormEditar({
            ...formEditar,
            [target.name]:target.value,
        })
    }
    //Funcion para eliminar post
    const handleDeletePost= (id)=>{
        dispatch(deletePost(id));
    }
    //Función para el submit
    const handleSubmitEditar = (e)=>{
        e.preventDefault();
        console.log(formEditar);
    }
    return (
        <div>
        <Card>
                <Avatar 
                    alt="Remy Sharp" 
                    src={userEdit.avatar} 
                    className={classes.large} />
                <CardContent>
                <form onSubmit={handleSubmitEditar}>
                    <TextField
                        value={name}
                        name="name"
                        onChange={handleInputChage}
                    />
                    <TextField
                        value={last}
                        name="last"
                        onChange={handleInputChage}
                    />
                    <TextField
                        value={email}
                        name="email"
                        onChange={handleInputChage}
                    />
                    <TextField
                        placeholder="Job"
                        value={job}
                        name="job"
                        onChange={handleInputChage}
                    />
                    <CardActions>
                        <Button 
                            type="submitr"
                            size="small" 
                            color="primary">
                                Guardar cambios
                        </Button>
                    </CardActions>
                    </form>
            </CardContent>
        </Card>
        <Typography variant="h5">
            Post del usuario:
        </Typography>
        <List>
        {
            posts.map(post=>(
                <ListItem  key={post.id}>
                <Typography>Eliminar Post</Typography>
                <Tooltip title="Eliminar">
                    <IconButton onClick={()=>handleDeletePost(post.id)} >
                        <Delete/>
                    </IconButton>
                </Tooltip>
                <ListItemText
                primary={post.title}
                secondary={
                    <React.Fragment>
                    <Typography
                        component="span"
                        variant="body2"
                        color="textPrimary"
                    >
                        {post.body}
                    </Typography>
                    </React.Fragment>
                } 
                />
              </ListItem>
            ))
        }
        </List>
        </div>
    )
}
