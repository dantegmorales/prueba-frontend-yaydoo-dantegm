import { Avatar, Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid, makeStyles, Paper, TextField, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { loadUsers, setUserSelected } from '../../actions/usersActions';
import { EditarUsuario } from './EditarUsuario';

const useStyles = makeStyles((theme)=>({
    card: {
      maxWidth: 345,
      marginBottom:'30px',
    },
    large: {
        width: theme.spacing(7),
        height: theme.spacing(7),
      },
      root: {
        flexGrow: 1,
      },
  }));

export const Usuarios = () => {
    //Instanciamos estilos
    const classes = useStyles();
    //Instanciamos el dispatch
     const dispatch = useDispatch();
     //Usamos el hook useSelector para traer datos del storage
     const {users} = useSelector(state=>state.users);
    //Efecto para disparar la petición de redux
    useEffect(() => {
        dispatch(loadUsers());
    }, []);
    //State para usuario a editar
    const [userEdit, setuserEdit] = useState({});
    //Funcion para seleccionar usuario
    const userSelect = (user)=>{
        setuserEdit(user);
        dispatch(setUserSelected(user));
    }

    console.log('usuarios:',users)
    return (
        <div className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
          {
                        users.map(user=>(
                            <Card key={user.id} className={classes.card}>
                                <CardActionArea>
                                    <Avatar 
                                        alt="Remy Sharp" 
                                        src={user.avatar} 
                                        className={classes.large} />
                                    <CardContent>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {user.first_name}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {user.last_name}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {user.email}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {user.job}
                                    </Typography>
                                    </CardContent>
                                </CardActionArea>
                                <CardActions>
                                    <Button 
                                        onClick={()=>userSelect(user)}
                                        size="small" 
                                        color="primary">
                                    Editar
                                    </Button>
                                </CardActions>
                                </Card>
                        ))
                    }
          </Grid>
          <Grid item xs={6}>
                <Typography variant="h5">Usuario Seleccionado:</Typography>
                {
                    (userEdit.id)?
                    (<EditarUsuario userEdit={userEdit} />):
                    (<Typography variant="subtitle2">
                        No se ha seleccionando ningun usuario
                    </Typography>)
                }
          </Grid>
        </Grid>
      </div>
    )
}
