import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { SeccionesRoutes } from '../../router/SeccionesRoutes';
import { NavLink } from 'react-router-dom';
import { Group,PhotoAlbum,ExitToApp } from '@material-ui/icons';
import { Tooltip,Avatar, } from '@material-ui/core';
import { logout } from '../../actions/autenticacion';
import { useDispatch } from 'react-redux';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  logout:{
    position:'absolute',
    width: '100%',
  },
}));

export const  SideBarMenu = (props)=>{
  //Instanciamos el dispatch 
  const dispatch = useDispatch();

  const { window } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  //Función para cerrar sesión
  const handleLogOut = ()=>{
      dispatch(logout());
  }

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
            <ListItem >
              <Avatar
                 alt="Logo" 
                 src="https://image.shutterstock.com/image-vector/childish-seamless-pattern-aliens-faces-600w-1494812873.jpg" />
                 <Typography variant="h6">Dante G M</Typography>
            </ListItem>
        <NavLink style={{textDecoration:'none'}} to={`/usuarios`}>
            <ListItem button>
                  <Tooltip title="Home">
                    <ListItemIcon>
                      <Group />
                    </ListItemIcon>
                  </Tooltip>
                  <ListItemText primary="Home" />
            </ListItem>
        </NavLink>
        <NavLink style={{textDecoration:'none'}} to={`/albums`}>
            <ListItem button>
                  <Tooltip title="Album">
                    <ListItemIcon>
                      <PhotoAlbum />
                    </ListItemIcon>
                  </Tooltip>
                  <ListItemText primary="Album" />
            </ListItem>
        </NavLink>
            <ListItem className={classes.logout}
            children
            onClick={handleLogOut} 
            button>
                  <Tooltip title="Salir">
                    <ListItemIcon>
                      <ExitToApp />
                    </ListItemIcon>
                  </Tooltip>
                  <ListItemText primary="Salir" />
            </ListItem>
        
      </List>
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
            <SeccionesRoutes />
      </main>
    </div>
  );
}

SideBarMenu.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default SideBarMenu;