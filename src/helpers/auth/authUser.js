
export const authUser = async (email,password) =>{
    
    //Url 
    const url = `https://reqres.in/api/login`;
    //usamos la función fetch
    const response = await fetch(url,{
        method:'POST',
        body:JSON.stringify({email,password}),
        headers:{
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    });
    //Retorna la respuesta en formato JSON
    const resp = await response.json();
    return resp;

}