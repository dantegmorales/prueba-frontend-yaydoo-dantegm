export const getUsers = async ()=>{
    const users=[];
    const url = `https://reqres.in/api/users`;
    const resp = await fetch(url,{
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
          }
    })
    const {data} = await resp.json();
    console.log('resultado:',data);
    if(!data) return users;
    data.forEach(user => {
        users.push({
            ...user
        })
    });
    return users;
}

export const getPostByUser = async (userId)=>{
    const url = `https://jsonplaceholder.typicode.com/posts?`+ new URLSearchParams({
        userId
    });
    const resp = await fetch(url,{
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
          }
    })
    const result =  await resp.json();
    return result;
}

export const getAlbumsByUser = async (id)=>{
    const url = `https://jsonplaceholder.typicode.com/users/${id}/albums`;
    const resp = await fetch(url,{
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
          }
    })
    const result =  await resp.json();
    return result;
}