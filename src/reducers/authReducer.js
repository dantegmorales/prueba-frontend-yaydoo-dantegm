//Importamos los tipos
import { types } from "../types/types";

//Declaramos el objeto con el estado incial del reducer
const initialState = {
    logged:false,
}

//Reducer de autenticación
export const authReducer = ( state = initialState, action )=>{
    switch (action.type) {
        case types.authLogin:
             return {
                 ...state,
                 email:action.payload.email,
                 token:action.payload.token,
                 logged:true,
             }
        case types.authLogout:
            return {
                logged:false
            }
        default:
            return state;
    }
}