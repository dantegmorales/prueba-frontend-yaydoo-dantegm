import { types } from "../types/types"

const initialState={
    posts:[]
}

export const postReducer = ( state = initialState, action )=>{
    switch(action.type){
        case types.postsAdd:
            return{
                ...state,
                posts:[ ...action.payload ]
            }
        case types.postsEdit:
                return {
                    ...state,
                    posts: state.posts.map(
                        post => post.id === action.payload.id
                            ? action.payload.post
                            : post
                    )
                }
        case types.postsDelete:
            return{
                ...state,
                posts: state.posts.filter( post => post.id !== action.payload )
            }
        default:
            return state;
    }
}