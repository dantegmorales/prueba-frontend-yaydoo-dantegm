import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { postReducer } from "./postReducer";
import { usersReducer } from "./usersReducer";

export const rootReducer = combineReducers({
    auth:authReducer,
    users:usersReducer,
    posts:postReducer,
});