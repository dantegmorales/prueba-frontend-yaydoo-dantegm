import { types } from "../types/types"

const initialState={
    users:[],
    userSelected:{}
}

export const usersReducer = ( state = initialState, action )=>{
    switch(action.type){
        case types.usuariosAdd:
            return{
                ...state,
                users:[ ...action.payload ]
            }
        case types.usuariosEdit:
                return {
                    ...state,
                    users: state.users.map(
                        user => user.IdUsuario === action.payload.id
                            ? action.payload.user
                            : user
                    )
                }
        case types.usuariosDelete:
            return{
                ...state,
                users: state.users.filter( user => user.IdUsuario !== action.payload )
            }
        case types.usuarioSelected:
                return{
                    ...state,
                    userSelected:action.payload,
                }
        default:
            return state;
    }
}