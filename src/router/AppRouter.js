import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";
import { PrivateRoute } from "./PrivateRoute";
import { PublicRoute } from "./PublicRoute";
import { AuthRoutes } from "./AuthRoutes";
import { DashboardRoutes } from "./DasboardRoutes";
import { useDispatch, useSelector } from "react-redux";

export const AppRouter = () => {

    //Instanciamos el dispatch
    const dispatch = useDispatch();
    //Usamos el selector para obtener los datos del storage
    const { logged } = useSelector(state => state.auth);
    return (
        <Router>
            <div>
            <Switch>
                <PublicRoute
                path="/login"
                component={AuthRoutes}
                isAuthenticated={logged}
                />
                <PrivateRoute 
                path="/"
                component={DashboardRoutes}
                isAuthenticated={logged}
                />
                <Redirect 
                to="/login"
                />
            </Switch>
            </div>
        </Router>
    )
}
