import React from 'react'
import {
    Switch,
    Route,
    Redirect
  } from "react-router-dom";
import { Login } from '../components/auth/Login';

export const AuthRoutes = () => {
    return (
       <div>
           <Switch>
               <Route 
               path="/login"
               component={Login}
                />
               <Redirect 
               to="/login"
               />
           </Switch>
       </div>
    )
}
