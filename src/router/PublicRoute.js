import React from 'react';
import PropTypes from 'prop-types';

import { Route, Redirect } from 'react-router-dom';


export const PublicRoute = ({
    isAuthenticated,
    component: Component,
    ...rest
}) => {

    const lastRoute = localStorage.getItem('lastPath');

    if(lastRoute){
        return (
            <Route { ...rest }
                component={ (props) => (
                    ( !isAuthenticated )
                        ? ( <Component { ...props } /> )
                        : ( <Redirect to={lastRoute} /> )
                )}
            
            />
        )

    }else{
        return (
            <Route { ...rest }
                component={ (props) => (
                    ( !isAuthenticated )
                        ? ( <Component { ...props } /> )
                        : ( <Redirect to="/Inicio" /> )
                )}
            
            />
        )

    }
}

PublicRoute.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired
}
