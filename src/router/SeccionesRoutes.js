import React from 'react';
import { Route, Switch } from "react-router";
import { Usuarios } from "../components/private/Usuarios";
import { Album } from "../components/private/Album";

export const SeccionesRoutes = () => {
    return (
        <div>
            <Switch>
                <Route 
                path="/usuarios"
                component={Usuarios}
                 />
                 <Route 
                path="/albums"
                component={Album}
                 />
            </Switch>
        </div>
    )
}
