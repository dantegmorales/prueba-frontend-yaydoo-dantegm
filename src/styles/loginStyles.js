import {makeStyles} from "@material-ui/core/styles";

export const loginStyles = makeStyles((theme)=>({
    root: {
        flexGrow: 1,
      },
      paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      },
      container:{
        /* display: 'flex',
        alignItems: 'center', */
        backgroundColor:'#5e91f8',
       /*  height:'100vh',
        width:'100vw', */
        margin:0,
      },
      inputs:{
        marginBottom:'20px'
      },
      titulo:{
        color:'white',
        marginTop:'90px',
        marginBottom:'30px'
      },
      footer:{
        color:'white',
        marginTop:'40px',
        marginBottom:'50px',
      },
      footerContainer:{
        padding:0,
        margin:0,
      },
      formContainer:{
        paddingTop:'80px',
        paddingLeft:'60px',
        paddingRight:'60px',
        paddingBottom:'40px'
      },
      btnLogin:{
        padding:'15px',
      }
}))