import { createMuiTheme } from "@material-ui/core/styles";
import 'fontsource-roboto';



export const themeConfig = createMuiTheme({
    typography:{
        fontFamily: "Roboto"
    },
    palette:{
        primary:{
            //light:'#454d58',
            main:'#5e91f8',
            //dark:'#101720',
        }
    }
});