//Objeto que contiene los tipos para manejo de los Reducers
export const types = {
//Types para autenticación
authLogin: '[auth] Login',
authLogout: '[auth] Logout',
//Types para usuarios
usuariosAdd: '[usuarios] Add',
usuariosGet: '[usuarios] Get',
usuariosEdit: '[usuarios] Edit',
usuariosDelete: '[usuarios] Delete',
usuarioSelected: '[usuarios] Select',
//Types para posts
postsAdd: '[posts] Add',
postsGet: '[posts] Get',
postsEdit: '[posts] Edit',
postsDelete: '[posts] Delete',
//Types par albums
albumsAdd: '[albums] Add',
albumsGet: '[albums] Get',
albumsEdit: '[albums] Edit',
albumsDelete: '[albums] Delete',
}